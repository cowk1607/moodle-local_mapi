# Local Moodle API

## Funtionality
- Get a paginated list of users
- Get a paginated list of courses
- Get a full list of enrolled users for the specific course

Before using make sure:
- you have enable web services;
- you have enable REST protocol;
- you have created a user with the webservice/rest:use capability;
- you have created a token for the user.