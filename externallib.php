<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * External API.
 *
 * @package    local_mapi
 */

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/externallib.php');
require_once($CFG->dirroot . '/local/mapi/lib.php');

/**
 * External functions.
 *
 * @package    local_mapi
 */
class local_mapi_external extends external_api
{
    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     */
    public static function get_users_list_parameters()
    {
        return new external_function_parameters(
            array (
                'start' => new external_value(PARAM_INT, 'Start from', VALUE_DEFAULT, 0),
                'count' => new external_value(PARAM_INT, 'Users count', VALUE_DEFAULT, 0),
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     */
    public static function get_courses_list_parameters()
    {
        return new external_function_parameters(
            array (
                'start' => new external_value(PARAM_INT, 'Start from', VALUE_DEFAULT, 0),
                'count' => new external_value(PARAM_INT, 'Courses count', VALUE_DEFAULT, 0),
            )
        );
    }

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     */
    public static function get_enrolled_users_parameters()
    {
        return new external_function_parameters(
            array (
                'id' => new external_value(PARAM_INT, 'Course ID', VALUE_REQUIRED),
            )
        );
    }

    /**
     * Returns description of method result value
     *
     * @return external_multiple_structure
     */
    public static function get_users_list_returns()
    {
        return new external_multiple_structure(
            new external_single_structure (
                array(
                    'id'        => new external_value(PARAM_INT, 'User ID'),
                    'firstname' => new external_value(PARAM_NOTAGS, 'The first name(s) of the user', VALUE_OPTIONAL),
                    'lastname'  => new external_value(PARAM_NOTAGS, 'The family name of the user', VALUE_OPTIONAL),
                    'email'     => new external_value(PARAM_TEXT, 'Email address', VALUE_OPTIONAL),
                )
            )
        );
    }

    /**
     * Returns description of method result value
     *
     * @return external_multiple_structure
     */
    public static function get_courses_list_returns()
    {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id'        => new external_value(PARAM_INT, 'Course ID'),
                    'shortname' => new external_value(PARAM_RAW, 'Course short name'),
                    'fullname'  => new external_value(PARAM_RAW, 'Course full name'),
                )
            )
        );
    }

    /**
     * Returns description of method result value
     *
     * @return external_multiple_structure
     */
    public static function get_enrolled_users_returns()
    {
        return new external_multiple_structure(
            new external_single_structure (
                array(
                    'id'            => new external_value(PARAM_INT, 'User ID'),
                    'firstname'     => new external_value(PARAM_NOTAGS, 'The first name(s) of the user', VALUE_OPTIONAL),
                    'lastname'      => new external_value(PARAM_NOTAGS, 'The family name of the user', VALUE_OPTIONAL),
                    'email'         => new external_value(PARAM_TEXT, 'Email address', VALUE_OPTIONAL),
                    'finalgrade'    => new external_value(PARAM_NUMBER, 'Final grade', VALUE_OPTIONAL),
                )
            )
        );
    }

    /**
     * Returns a list of users
     * 
     * @param int $start
     * @param int $count
     * @return array
     */
    public static function get_users_list(int $start = 0, int $count = 0): array
    {
        global $DB;

        $params = self::validate_parameters(
            self::get_users_list_parameters(),
            ['start' => $start, 'count' => $count]
        );

        return $DB->get_records('user', array(), 'lastname ASC', 'id, firstname, lastname, email', $start, $count);
    }

    /**
     * Returns a list of courses
     * 
     * @param int $start
     * @param int $count
     * @return array
     */
    public static function get_courses_list(int $start = 0, int $count = 0): array
    {
        global $DB;

        $params = self::validate_parameters(
            self::get_users_list_parameters(),
            ['start' => $start, 'count' => $count]
        );

        return $DB->get_records('course', array(), 'fullname ASC', 'id, shortname, fullname', $start, $count);
    }

    /**
     * Returns a list of enrolled users with grade.
     * 
     * @return array
     */
    public static function get_enrolled_users(int $courseid): array
    {
        global $DB;

        $data = [];

        $coursecontext = context_course::instance($courseid);

        list($enrolledsql, $enrolledparams) = get_enrolled_sql($coursecontext, '', 0, true);

        $sql = "SELECT u.id AS id, u.firstname AS firstname, u.lastname AS lastname, u.email AS email, gg.finalgrade AS finalgrade
                  FROM {grade_items} gi
                  JOIN {grade_grades} gg ON gg.itemid = gi.id
                  JOIN {user} u ON u.id = gg.userid
                  JOIN ($enrolledsql) je ON je.id = gg.userid
                 WHERE gi.courseid = :courseid
              GROUP BY u.id, gg.itemid";

        $enrolled = $DB->get_recordset_sql($sql, ['courseid' => $courseid] + $enrolledparams);

        foreach ($enrolled as $user) {
            $data[] = $user;
        }

        $enrolled->close();

        return $data;
    }
}