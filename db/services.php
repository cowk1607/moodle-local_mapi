<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Webservice definitions.
 *
 * @package local_mapi
 **/
defined('MOODLE_INTERNAL') || die();

$services = array(
    'mAPI service' => array(
        'functions' => array(
            'local_mapi_get_users_list',
            'local_mapi_get_courses_list',
        ),
        'requiredcapability' => 'webservice/rest:use',
        'restrictedusers' => 0,
        'enabled' => 1,
        'shortname' => 'mapi_service',
        'downloadfiles' => 0,
        'uploadfiles' => 0,
    )
);

$functions = array(
    'local_mapi_get_users_list' => array(
        'classname'     => 'local_mapi_external',
        'methodname'    => 'get_users_list',
        'classpath'     => 'local/mapi/externallib.php',
        'description'   => 'Retrieve the list of users.',
        'type'          => 'read',
        'ajax'          => true,
        'services'      => array('mapi_service'),
        'capabilities'  => 'webservice/rest:use',
    ),
    'local_mapi_get_courses_list' => array(
        'classname'     => 'local_mapi_external',
        'methodname'    => 'get_courses_list',
        'classpath'     => 'local/mapi/externallib.php',
        'description'   => 'Retrieve the list of courses.',
        'type'          => 'read',
        'ajax'          => true,
        'services'      => array('mapi_service'),
        'capabilities'  => 'webservice/rest:use',
    ),
    'local_mapi_get_enrolled_users' => array(
        'classname'     => 'local_mapi_external',
        'methodname'    => 'get_enrolled_users',
        'classpath'     => 'local/mapi/externallib.php',
        'description'   => 'Retrieve the list of enrolled users.',
        'type'          => 'read',
        'ajax'          => true,
        'services'      => array('mapi_service'),
        'capabilities'  => 'webservice/rest:use',
    ),
);